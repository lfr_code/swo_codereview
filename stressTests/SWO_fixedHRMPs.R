# SWO_fixedHRMPs.R

SWO_hr_q <- function(x, Data, hr = 0.00, q = 0.0015, ...) 
{
  yr          <- length(Data@Year)
  

  if(all(!is.na(Data@SpAbun))) {
    biomass <- Data@SpAbun[x]
  }
  else {
    idxSeries   <- Data@Ind[x,]
    if(is.na(idxSeries[yr]))
      yr <- max(which(!is.na(idxSeries)))
    
    biomass <- idxSeries[yr]/q
  }
  
  Rec <- new("Rec")
  Rec@TAC <- biomass*hr

  Rec
}
class(SWO_hr_q) <- 'MP'


makeGridHRMPs <- function(  hrGrid = seq(from = 0, to = 0.9, by = 0.01),
                            outFile = "autoHRgridMPs.R",
                            baseMP = SWO_hr_q )
{ 
  cat("# Automatically generated MPs for SWO stress test.\n", file = outFile)
  cat("\n", file = outFile, append = TRUE)
  cat("\n", file = outFile, append = TRUE)
  cat("\n", file = outFile, append = TRUE)

  for( gridIdx in 1:length(hrGrid) )
  {
    hr <- hrGrid[gridIdx]

    mpName <- paste0("SWO_fixhr",hr)

    cat("# hr = ", hr, "\n", file = outFile, sep = "", append = TRUE)
    cat(mpName," <- ", baseMP, "\n", sep = "", file = outFile, append = TRUE)
    cat("formals(",mpName,")$hr <- ", hr, "\n", sep = "", file = outFile, append = TRUE)
    cat("class(",mpName,") <- 'MP' \n",sep = "", file = outFile, append = TRUE)
    cat("\n", file = outFile, append = TRUE)
    cat("\n", file = outFile, append = TRUE)

  }

  cat("# End automatically generated grid \n", sep = "", file = outFile, append = TRUE)

} # END makeGridHRMPs



