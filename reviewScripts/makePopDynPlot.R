
# Load MSE object
library(SWOMSE)
mseObj <- readRDS("../stressTests/MSEs/fixedC/OM_1.rds")

R0 <- mseObj@Hist@OMPars$R0[1]
selAge <- mseObj@Hist@AtAge$Select[1,,1]

M <- 0.1
Ftest <- c(0,0.05,0.1,0.2,0.5,1.0,2.0)

nFs <- length(Ftest)
A <- 26
nT <- 40

Z_fa <- array(M, dim = c(length(Ftest),A) )
for( f in 1:length(Ftest))
  Z_fa[f,] <- M + Ftest[f] * selAge


t <- seq(from = 1, to = nT, by = 1 )


l1_za <- array(1, dim = c( nFs, A ) )
l2_za <- array(1, dim = c( nFs, A ) )

for( zIdx in 1:nFs )
  for( a in 2:A )
  {
    l1_za[zIdx,a] <- l1_za[zIdx,a-1] * exp(-Z_fa[zIdx,a-1])
    l2_za[zIdx,a] <- l2_za[zIdx,a-1] * exp(-Z_fa[zIdx,a-1])

    if(a == A )
    {
      l1_za[zIdx,a] <- l1_za[zIdx,a]/(1 - exp(-Z_fa[zIdx,a]))
      l2_za[zIdx,a] <- l2_za[zIdx,a] + l2_za[zIdx,a] * exp(-Z_fa[zIdx,a])/(1 - exp(-Z_fa[zIdx,a]))
    }
  }

Rt <- R0 * exp( rnorm(nT, mean = 0,sd = 0.6) - 0.5 * 0.6^2 )
Rinit <- R0 * exp( rnorm(A, mean = 0,sd = 0.6) - 0.5 * 0.6^2 )
N1_zat <- array(1, dim = c( nFs, A, length(t) ) )
N2_zat <- array(1, dim = c( nFs, A, length(t) ) )




for(zIdx in 1:nFs)
{  
  N1_zat[zIdx,,1] <- Rinit * l1_za[zIdx,]
  N2_zat[zIdx,,1] <- Rinit * l2_za[zIdx,]
  for( tIdx in 2:length(t) )
  {
    N1_zat[zIdx,1,tIdx] <- N2_zat[zIdx,1,tIdx] <- Rt[tIdx]
    
    for( a in 2:A )
    {
      N1_zat[zIdx,a,tIdx] <- N1_zat[zIdx,a-1,tIdx-1] * exp(-Z_fa[zIdx,a-1])
      N2_zat[zIdx,a,tIdx] <- N2_zat[zIdx,a-1,tIdx-1] * exp(-Z_fa[zIdx,a-1])

      if(a == A )
      {
        N1_zat[zIdx,a,tIdx] <- N1_zat[zIdx,a,tIdx] + N1_zat[zIdx,a,tIdx-1]* exp(-Z_fa[zIdx,a])
        N2_zat[zIdx,a,tIdx] <- N2_zat[zIdx,a,tIdx] + N2_zat[zIdx,a,tIdx] * exp(-Z_fa[zIdx,a])/(1 - exp(-Z_fa[zIdx,a]))
      }
    }
  }
}

nCols <- nFs
plotCols <- RColorBrewer::brewer.pal(nCols,"Dark2")

par(mfrow = c(nFs,2), mar = c(.1,2,.1,2), oma = c(5,5,1,1) )

for( zIdx in 1:nFs )
{
  # First, show equilibrium difference

  plot( x = c(0,A-1), y = c(0,max(l1_za,l2_za)), axes = FALSE, type = "n")
    mfg <- par("mfg")
    grid()
    box()
    axis(side = 2,las = 1)
    if(mfg[1] == 1)
      mtext( side = 3, text = "Equilibrium Survival", font = 2)

    if(mfg[1] == mfg[3])
    {
      axis(side = 1 )
      mtext(side = 1, text = "Age", line = 2)
    }
    # First plot correct spec
    rect(xleft = 0:(A-1) - 0.3, xright = 0:(A-1),
          ybottom = 0, ytop = l1_za[zIdx,], col = "grey40", border = NA )
    # Then incorrect spec
    rect(xleft = 0:(A-1), xright = 0:(A-1) + 0.3,
          ybottom = 0, ytop = l2_za[zIdx,], col = "salmon", border = NA )

  # Now show time-varying differenc
  plot( x = c(1,max(t)), y = c(0,max(N1_zat[zIdx,A,]/N2_zat[zIdx,A,])),
          type = "n", las = 1, axes = FALSE )
    mfg <- par("mfg")
    grid()
    box()
    axis(side = 2,las = 1)
    if(mfg[1] == 1)
      mtext( side = 3, text = "Plus group size relative to correct specification", font = 2)
    if(mfg[1] == mfg[3])
    {
      axis(side = 1 )
      mtext(side = 1, text = "Year", line = 2)
    }
    lines( x = t, y = rep(1,nT), col = "grey40", lty = 1, lwd = 3)
    lines( x = t, y = N2_zat[zIdx,A,]/N1_zat[zIdx,A,], col = "salmon", lty = 2, lwd = 3)

  mtext( side = 4, text = paste0("F = ",Ftest[zIdx]), line = 2 )

  # legend( x = "topright", bty = "n",
  #         legend = c(paste0("Z = ",Z[zIdx]), "Correct", "Incorrect"),
  #         lty = c(NA,1,2),
  #         lwd = c(NA,2,2),
  #         col = c(NA,plotCols[zIdx],plotCols[zIdx]) )
}

# mtext( side = 1, text = "Year", outer = TRUE, line = 2)
mtext( side = 2, text = "Proportion", outer = TRUE, line = 2)

